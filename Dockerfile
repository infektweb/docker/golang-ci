FROM registry.gitlab.com/infektweb/docker/gobuild:latest

RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b /usr/bin v1.49.0
